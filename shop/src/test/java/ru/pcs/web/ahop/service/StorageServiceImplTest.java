package ru.pcs.web.ahop.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.pcs.web.ahop.ShopApplicationTests;
import ru.pcs.web.ahop.forms.ProductForm;
import ru.pcs.web.ahop.forms.StorageForm;
import ru.pcs.web.ahop.models.Product;
import ru.pcs.web.ahop.models.Storage;
import ru.pcs.web.ahop.services.StoragesServiceImpl;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class StorageServiceImplTest extends ShopApplicationTests {

    @Autowired
    private StoragesServiceImpl storagesService;

    StorageForm storageForm = StorageForm.builder()
            .name("Сектор 4")
            .amount_order(200)
            .amount_storekeeper(16)
            .build();

    @Test
    void testGetAllStorages() {
        List<Storage> storageList = storagesService.getAllStorage();
        assertNotNull(storageList);
    }

    @Test
    void testGetStorage() {

        Storage createdStorage = storagesService.addStorage(storageForm);
        Storage storage = storagesService.getStorage(createdStorage.getId());

        assertNotNull(storage);
        assertEquals(storage.getId(), createdStorage.getId());
        assertEquals(storage.getName(), createdStorage.getName());
        assertEquals(storage.getAmount_order(), createdStorage.getAmount_order());
        assertEquals(storage.getAmount_storekeeper(), createdStorage.getAmount_storekeeper());
    }

    @Test
    void testAddStorage() {
        Storage createStorage = storagesService.addStorage(storageForm);

        assertNotNull(createStorage.getId());
        assertEquals(storageForm.getName(), createStorage.getName());
        assertEquals(storageForm.getAmount_order(), createStorage.getAmount_order());
        assertEquals(storageForm.getAmount_storekeeper(), createStorage.getAmount_storekeeper());
    }

    @Test
    void testUpdateStorage() {
        StorageForm storageFormUpdate = StorageForm.builder()
                .name("Сектор 1")
                .amount_order(144)
                .amount_storekeeper(7)
                .build();
        Storage oldStorage = storagesService.addStorage(storageForm);
        Storage newStorage = storagesService.updateStorage(oldStorage.getId(), storageFormUpdate);

        assertEquals(oldStorage.getId(), newStorage.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(oldStorage.getName(), newStorage.getName());
        assertNotEquals(oldStorage.getAmount_order(), newStorage.getAmount_order());
        assertNotEquals(oldStorage.getAmount_storekeeper(), newStorage.getAmount_storekeeper());
    }

    @Test
    void testDeleteStorage() {
        Storage createdStorage = storagesService.addStorage(storageForm);
        storagesService.deleteStorage(createdStorage.getId());
    }

}
