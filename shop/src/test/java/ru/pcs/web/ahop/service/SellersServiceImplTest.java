package ru.pcs.web.ahop.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.pcs.web.ahop.ShopApplicationTests;
import ru.pcs.web.ahop.forms.CustomerForm;
import ru.pcs.web.ahop.forms.ProductForm;
import ru.pcs.web.ahop.forms.SellerForm;
import ru.pcs.web.ahop.models.Customer;
import ru.pcs.web.ahop.models.Product;
import ru.pcs.web.ahop.models.Seller;
import ru.pcs.web.ahop.services.ProductsServiceImpl;
import ru.pcs.web.ahop.services.SellersServiceImpl;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SellersServiceImplTest extends ShopApplicationTests {
    @Autowired
    private SellersServiceImpl sellersService;
    @Autowired
    private ProductsServiceImpl productsService;

    SellerForm sellerForm = SellerForm.builder()
            .entity("ИП Бледных")
            .phone("+79991234567")
            .email("test@mail.ru")
            .adress("Азино, дом 25")
            .build();

    ProductForm productForm = ProductForm.builder()
            .name("apple")
            .cost(22)
            .amount(24)
            .build();

    @Test
    void testGetAllSellers() {
        List<Seller> sellers = sellersService.getAllSellers();
        assertNotNull(sellers);
    }

    @Test
    void testGetSeller() {

        Seller createdSeller = sellersService.addSeller(sellerForm);
        Seller seller = sellersService.getSeller(createdSeller.getId());

        assertNotNull(seller);
        assertEquals(seller.getEntity(), createdSeller.getEntity());
        assertEquals(seller.getPhone(), createdSeller.getPhone());
        assertEquals(seller.getEmail(), createdSeller.getEmail());
        assertEquals(seller.getAdress(), createdSeller.getAdress());
    }

    @Test
    void testAddSeller() {
        Seller createSeller = sellersService.addSeller(sellerForm);

        assertNotNull(createSeller.getId());
        assertEquals(sellerForm.getEntity(), createSeller.getEntity());
        assertEquals(sellerForm.getPhone(), createSeller.getPhone());
        assertEquals(sellerForm.getEmail(), createSeller.getEmail());
        assertEquals(sellerForm.getAdress(), createSeller.getAdress());
    }

    @Test
    void testUpdateSeller() {
        SellerForm sellerFormUpdate = SellerForm.builder()
                .entity("ИП Фирсанов")
                .phone("+79951234567")
                .email("firs@mail.ru")
                .adress("Спартаковская, дом 25")
                .build();
        Seller oldSeller = sellersService.addSeller(sellerForm);
        Seller newSeller = sellersService.updateSeller(oldSeller.getId(), sellerFormUpdate);

        assertEquals(oldSeller.getId(), newSeller.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(oldSeller.getEntity(), newSeller.getEntity());
        assertNotEquals(oldSeller.getPhone(), newSeller.getPhone());
        assertNotEquals(oldSeller.getEmail(), newSeller.getEmail());
        assertNotEquals(oldSeller.getAdress(), newSeller.getAdress());
    }

    @Test
    void testDeleteSeller() {
        Seller createdSeller = sellersService.addSeller(sellerForm);
        sellersService.deleteSeller(createdSeller.getId());
    }

    @Test
    void testAddProductToSeller() {
        Product product = productsService.addProduct(productForm);
        Seller seller = sellersService.addSeller(sellerForm);
        product = sellersService.addProductToSeller(seller.getId(), product.getId());

        assertNotNull(product.getSeller());
        assertEquals(seller.getEntity(), product.getSeller().getEntity());
        assertEquals(seller.getPhone(), product.getSeller().getPhone());
        assertEquals(seller.getEmail(), product.getSeller().getEmail());
        assertEquals(seller.getAdress(), product.getSeller().getAdress());
    }

    @Test
    void testDeleteProductToSeller() {
        Product product = productsService.addProduct(productForm);
        Seller seller = sellersService.addSeller(sellerForm);

        product = sellersService.addProductToSeller(seller.getId(), product.getId());
        assertNotNull(product.getSeller());

        product = sellersService.deleteProductToSeller(product.getId());
        assertNull(product.getSeller());
    }

    @Test
    void testGetProductsBySeller() {
        Product product = productsService.addProduct(productForm);
        Seller seller = sellersService.addSeller(sellerForm);

        product = sellersService.addProductToSeller(seller.getId(), product.getId());
        List<Product> products = sellersService.getProductsBySeller(product.getId());
        assertNotNull(products);
    }

    @Test
    void testGetProductWithoutSeller() {
        Product product = productsService.addProduct(productForm);
        List<Product> products = sellersService.getProductWithoutSeller();
        assertNotNull(products);
    }
}
