package ru.pcs.web.ahop.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.pcs.web.ahop.ShopApplicationTests;
import ru.pcs.web.ahop.forms.CustomerForm;
import ru.pcs.web.ahop.forms.ProductForm;
import ru.pcs.web.ahop.forms.StorekeeperForm;
import ru.pcs.web.ahop.models.Customer;
import ru.pcs.web.ahop.models.Product;
import ru.pcs.web.ahop.models.Seller;
import ru.pcs.web.ahop.models.Storekeeper;
import ru.pcs.web.ahop.services.CustomersServiceImpl;
import ru.pcs.web.ahop.services.ProductsService;
import ru.pcs.web.ahop.services.ProductsServiceImpl;
import ru.pcs.web.ahop.services.StorekeeperServiceImpl;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StorekeeperServiceImplTest extends ShopApplicationTests {
    @Autowired
    private StorekeeperServiceImpl storekeeperService;
    @Autowired
    private ProductsService productsService;
    @Autowired
    private CustomersServiceImpl customersService;
    StorekeeperForm storekeeperForm = StorekeeperForm.builder()
            .first_Name("Евгений")
            .last_Name("Бледных")
            .phone("+79991234567")
            .build();

    ProductForm productForm = ProductForm.builder()
            .name("apple")
            .cost(22)
            .amount(24)
            .build();

    CustomerForm customerForm = CustomerForm.builder()
            .first_Name("Евгений")
            .last_Name("Бледных")
            .phone("+79991234567")
            .email("edblednykh@mail.ru")
            .adress("Азино, дом 25")
            .build();

    @Test
    void testGetAllStorekeepers() {
        List<Storekeeper> storekeeper = storekeeperService.getAllStorekeeper();
        assertNotNull(storekeeper);
    }

    @Test
    void testGetStorekeeper() {

        Storekeeper createdStorekeeper = storekeeperService.addStorekeeper(storekeeperForm);
        Storekeeper storekeeper = storekeeperService.getStorekeeper(createdStorekeeper.getId());

        assertNotNull(storekeeper);
        assertEquals(storekeeper.getFirst_Name(), createdStorekeeper.getFirst_Name());
        assertEquals(storekeeper.getLast_Name(), createdStorekeeper.getLast_Name());
        assertEquals(storekeeper.getPhone(), createdStorekeeper.getPhone());
    }

    @Test
    void testAddStorekeeper() {
        Storekeeper createStorekeeper = storekeeperService.addStorekeeper(storekeeperForm);

        assertNotNull(createStorekeeper.getId());
        assertEquals(storekeeperForm.getFirst_Name(), createStorekeeper.getFirst_Name());
        assertEquals(storekeeperForm.getLast_Name(), createStorekeeper.getLast_Name());
        assertEquals(storekeeperForm.getPhone(), createStorekeeper.getPhone());
    }

    @Test
    void testUpdateStorekeeper() {
        StorekeeperForm storekeeperFormUpdate = StorekeeperForm.builder()
                .first_Name("Анатолий")
                .last_Name("Фирсанов")
                .phone("+79951234567")
                .build();
        Storekeeper oldStorekeeper = storekeeperService.addStorekeeper(storekeeperForm);
        Storekeeper newStorekeeper = storekeeperService.updateStorekeeper(oldStorekeeper.getId(), storekeeperFormUpdate);

        assertEquals(oldStorekeeper.getId(), newStorekeeper.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(oldStorekeeper.getFirst_Name(), newStorekeeper.getFirst_Name());
        assertNotEquals(oldStorekeeper.getLast_Name(), newStorekeeper.getLast_Name());
        assertNotEquals(oldStorekeeper.getPhone(), newStorekeeper.getPhone());
    }

    @Test
    void testDeleteStorekeeper() {
        Storekeeper createdStorekeeper = storekeeperService.addStorekeeper(storekeeperForm);
        storekeeperService.deleteStorekeeper(createdStorekeeper.getId());
    }

    @Test
    void addProductToStorekeeper() {
        Product product = productsService.addProduct(productForm);
        Storekeeper storekeeper = storekeeperService.addStorekeeper(storekeeperForm);
        product = storekeeperService.addProductToStorekeeper(storekeeper.getId(), product.getId());

        assertNotNull(product.getStorekeeper());
        assertEquals(storekeeper.getFirst_Name(), product.getStorekeeper().getFirst_Name());
        assertEquals(storekeeper.getLast_Name(), product.getStorekeeper().getLast_Name());
        assertEquals(storekeeper.getPhone(), product.getStorekeeper().getPhone());

    }

    @Test
    void testDeleteProductToStorekeeperAndCustomer() {
        Product product = productsService.addProduct(productForm);
        Customer customer = customersService.addCustomer(customerForm);
        Storekeeper storekeeper = storekeeperService.addStorekeeper(storekeeperForm);


        product = storekeeperService.addProductToStorekeeper(storekeeper.getId(), product.getId());
        product = customersService.addProductToCustomer(customer.getId(), product.getId());
        assertNotNull(product.getStorekeeper());
        assertNotNull(product.getCustomer());

        product = storekeeperService.deleteProductToStorekeeperAndCustomer(product.getId());
        assertNull(product.getStorekeeper());
        assertNull(product.getCustomer());
    }

    @Test
    void testGetProductByCustomerAndStorekeeper() {
        Product product = productsService.addProduct(productForm);
        Customer customer = customersService.addCustomer(customerForm);
        Storekeeper storekeeper = storekeeperService.addStorekeeper(storekeeperForm);

        product = storekeeperService.addProductToStorekeeper(storekeeper.getId(), product.getId());
        product = customersService.addProductToCustomer(customer.getId(), product.getId());

        List<Product> products = storekeeperService.getProductByCustomerAndStorekeeper(product.getId());
        assertNotNull(products);
    }

    @Test
    void testGetProductWithoutSeller() {
        Product product = productsService.addProduct(productForm);
        Customer customer = customersService.addCustomer(customerForm);

        product = customersService.addProductToCustomer(customer.getId(), product.getId());

        List<Product> products = storekeeperService.getProductByCustomerWithoutStorekeeper();
        assertNotNull(products);
    }

}
