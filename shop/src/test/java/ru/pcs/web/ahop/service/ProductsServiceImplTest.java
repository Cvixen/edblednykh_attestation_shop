package ru.pcs.web.ahop.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import ru.pcs.web.ahop.ShopApplication;
import ru.pcs.web.ahop.ShopApplicationTests;
import ru.pcs.web.ahop.forms.ProductForm;
import ru.pcs.web.ahop.models.Product;
import ru.pcs.web.ahop.services.ProductsServiceImpl;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class ProductsServiceImplTest extends ShopApplicationTests {

    @Autowired
    private ProductsServiceImpl productsService;

    ProductForm productForm = ProductForm.builder()
            .name("apple")
            .cost(22)
            .amount(24)
            .build();

    @Test
    void testGetAllProducts() {
        List<Product> productList = productsService.getAllProducts();
        assertNotNull(productList);
    }

    @Test
    void testGetProduct() {

        Product createdProduct = productsService.addProduct(productForm);
        Product product = productsService.getProduct(createdProduct.getId());

        assertNotNull(product);
        assertEquals(product.getId(), createdProduct.getId());
        assertEquals(product.getName(), createdProduct.getName());
        assertEquals(product.getCost(), createdProduct.getCost());
        assertEquals(product.getAmount(), createdProduct.getAmount());
    }

    @Test
    void testAddProduct() {
        Product createProduct = productsService.addProduct(productForm);

        assertNotNull(createProduct.getId());
        assertEquals(productForm.getName(), createProduct.getName());
        assertEquals(productForm.getCost(), createProduct.getCost());
        assertEquals(productForm.getAmount(), createProduct.getAmount());
    }

    @Test
    void testUpdateProduct() {
        ProductForm productFormUpdate = ProductForm.builder()
                .name("icecube")
                .cost(21)
                .amount(20)
                .build();
        Product oldProduct = productsService.addProduct(productForm);
        Product newProduct = productsService.updateProduct(oldProduct.getId(), productFormUpdate);

        assertEquals(oldProduct.getId(), newProduct.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(oldProduct.getName(), newProduct.getName());
        assertNotEquals(oldProduct.getCost(), newProduct.getCost());
        assertNotEquals(oldProduct.getAmount(), newProduct.getAmount());
    }

    @Test
    void testDeleteProduct() {
        Product createdProduct = productsService.addProduct(productForm);
        productsService.deleteProduct(createdProduct.getId());
    }

}
