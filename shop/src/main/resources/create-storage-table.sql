create table storage
(
    id                 serial primary key,
    name               varchar(255),
    amount_order       integer check (amount_order > -1),
    amount_storekeeper integer check (amount_storekeeper > -1)
);

insert into storage(name, amount_order, amount_storekeeper)
values ('sadasd', 1, 2);
