create table customer
(
    id         serial primary key,
    first_Name varchar(255),
    last_Name  varchar(255),
    phone      varchar(255),
    email      varchar(255),
    adress     varchar(255)
);

insert into customer(first_Name, last_Name, phone, email, adress)
values ('Evgeniy', 'Blednykh', '+79991645222', 'lol@mail.ru', 'street baymana 28');
insert into customer(first_Name, last_Name, phone, email, adress)
values ('Tom', 'Volodarski', '+79991645222', 'lol@mail.ru', 'street baymana 28');
insert into customer(first_Name, last_Name, phone, email, adress)
values ('Michael', 'Jordon', '+79991645222', 'lol@mail.ru', 'street baymana 28');

