create table product
(
    id             serial primary key,
    name           varchar(20),
    cost           integer,
    amount         integer check (cost > 0),
    id_customer    integer,
    id_seller      integer,
    id_storekeeper integer,
    foreign key (id_customer) references customer (id),
    foreign key (id_seller) references seller (id),
    foreign key (id_storekeeper) references storekeeper (id)
);

insert into product(name, cost, amount)
values ('Apple', 56, 100);
insert into product(name, cost, amount)
values ('Orange', 70, 20);
insert into product(name, cost, amount)
values ('Banana', 40, 30);
