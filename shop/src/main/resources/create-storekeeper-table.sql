create table storekeeper
(
    id         serial primary key,
    first_Name varchar(255),
    last_Name  varchar(255),
    phone      varchar(255)
);

insert into storekeeper(first_Name, last_Name, phone)
values ('Антон', 'Касперский', '+79991645222');
insert into storekeeper(first_Name, last_Name, phone)
values ('Владимир', 'Нежданчик', '+79991645222');
