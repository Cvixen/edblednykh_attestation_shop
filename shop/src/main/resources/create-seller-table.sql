create table seller
(
    id     serial primary key,
    entity varchar(255),
    phone  varchar(255),
    email  varchar(255),
    adress varchar(255)
);

insert into seller(entity, phone, email, adress)
values ('ИП Федоров', '+79991552822', 'test@mail.ru', 'г.Спб,просп. Невского 22');
insert into seller(entity, phone, email, adress)
values ('ИП Бледных', '+79991552822', 'test@mail.ru', 'г.Спб,просп. Невского 22');
insert into seller(entity, phone, email, adress)
values ('ООО Земля', '+79991552822', 'test@mail.ru', 'г.Спб,просп. Невского 22');
