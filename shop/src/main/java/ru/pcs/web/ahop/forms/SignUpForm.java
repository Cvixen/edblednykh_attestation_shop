package ru.pcs.web.ahop.forms;

import lombok.Data;

@Data
public class SignUpForm {
    private String entity;
    private String phone;
    private String email;
    private String adress;
    private String password;
}
