package ru.pcs.web.ahop.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import ru.pcs.web.ahop.forms.SignUpForm;
import ru.pcs.web.ahop.models.Seller;
import ru.pcs.web.ahop.repositories.SellerRepository;

@Component
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final SellerRepository sellerRepository;

    @Override
    public void signUpSeller(SignUpForm form) {
        Seller seller = Seller.builder()
                .entity(form.getEntity())
                .phone(form.getPhone())
                .email(form.getEmail())
                .adress(form.getAdress())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .role(Seller.Role.USER)
                .build();
        sellerRepository.save(seller);
    }
}
