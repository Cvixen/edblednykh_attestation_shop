package ru.pcs.web.ahop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.ahop.forms.ProductForm;
import ru.pcs.web.ahop.forms.StorageForm;
import ru.pcs.web.ahop.models.Product;
import ru.pcs.web.ahop.models.Storage;
import ru.pcs.web.ahop.services.ProductsService;
import ru.pcs.web.ahop.services.StoragesService;

import java.util.List;

@Controller
public class StorageController {

    private final StoragesService storagesService;

    @Autowired
    public StorageController(StoragesService storagesService) {
        this.storagesService = storagesService;
    }

    @GetMapping("/storages")
    public String getStoragesPage(Model model) {
        List<Storage> storageList = storagesService.getAllStorage();
        model.addAttribute("storages", storageList);
        return "storages";
    }

    @GetMapping("/storages/{storage-id}")
    public String getStoragePage(Model model, @PathVariable("storage-id") Integer storageId) {
        Storage storage = storagesService.getStorage(storageId);
        model.addAttribute("storage", storage);
        return "storage";
    }

    @PostMapping("/storages")
    public String addStorage(StorageForm form) {
        storagesService.addStorage(form);
        return "redirect:/storages";
    }

    @PostMapping("/storages/{storage-id}/delete")
    public String deleteStorage(@PathVariable("storage-id") Integer storageId) {
        storagesService.deleteStorage(storageId);
        return "redirect:/storages";
    }


    @PostMapping("/storages/{storage-id}/update")
    public String updateStorage(@PathVariable("storage-id") Integer storageId, StorageForm form) {
        storagesService.updateStorage(storageId, form);
        return "redirect:/storages";
    }
}
