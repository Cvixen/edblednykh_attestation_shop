package ru.pcs.web.ahop.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StorekeeperForm {
    private String first_Name;
    private String last_Name;
    private String phone;
    private Integer id_Storage;
}
