package ru.pcs.web.ahop.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.pcs.web.ahop.models.Seller;
import ru.pcs.web.ahop.repositories.SellerRepository;


@RequiredArgsConstructor
@Component
public class SellersDetailsServiceImpl implements UserDetailsService {

    private final SellerRepository sellerRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Seller seller = sellerRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Seller not found"));
        return new SellersDetailsImpl(seller);
    }
}
