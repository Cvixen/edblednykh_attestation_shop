package ru.pcs.web.ahop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.ahop.forms.StorageForm;
import ru.pcs.web.ahop.forms.StorekeeperForm;
import ru.pcs.web.ahop.models.Product;
import ru.pcs.web.ahop.models.Storage;
import ru.pcs.web.ahop.models.Storekeeper;
import ru.pcs.web.ahop.services.StoragesService;
import ru.pcs.web.ahop.services.StorekeeperService;

import java.util.List;

@Controller
public class StorekeeperController {

    private final StorekeeperService storekeeperService;

    @Autowired
    public StorekeeperController(StorekeeperService storekeeperService) {
        this.storekeeperService = storekeeperService;
    }

    @GetMapping("/storekeepers")
    public String getStorekeeperPage(Model model) {
        List<Storekeeper> storekeeperList = storekeeperService.getAllStorekeeper();
        model.addAttribute("storekeepers", storekeeperList);
        return "storekeepers";
    }

    @GetMapping("/storekeepers/{storekeeper-id}")
    public String getStorekeeperPage(Model model, @PathVariable("storekeeper-id") Integer storekeeperId) {
        Storekeeper storekeeper = storekeeperService.getStorekeeper(storekeeperId);
        model.addAttribute("storekeeper", storekeeper);
        return "storekeeper";
    }

    @PostMapping("/storekeepers")
    public String addStorekeeper(StorekeeperForm form) {
        storekeeperService.addStorekeeper(form);
        return "redirect:/storekeepers";
    }

    @PostMapping("/storekeepers/{storekeeper-id}/delete")
    public String deleteStorekeeper(@PathVariable("storekeeper-id") Integer storekeeperId) {
        storekeeperService.deleteStorekeeper(storekeeperId);
        return "redirect:/storekeepers";
    }


    @PostMapping("/storekeepers/{storekeeper-id}/update")
    public String updateStorekeeper(@PathVariable("storekeeper-id") Integer storekeeperId, StorekeeperForm form) {
        storekeeperService.updateStorekeeper(storekeeperId, form);
        return "redirect:/storekeepers";
    }

    @GetMapping("/storekeepers/{storekeeper-id}/products")
    public String getProductsWithCustomerByStorekeepers(Model model, @PathVariable("storekeeper-id") Integer storekeeperId) {
        List<Product> productsWithStorekeeper = storekeeperService.getProductByCustomerAndStorekeeper(storekeeperId);
        List<Product> productsWithoutStorekeeper = storekeeperService.getProductByCustomerWithoutStorekeeper();
        model.addAttribute("storekeeperId", storekeeperId);
        model.addAttribute("productsWithStorekeeper", productsWithStorekeeper);
        model.addAttribute("productsWithoutStorekeeper", productsWithoutStorekeeper);
        return "storekeepers_with_products";
    }

    @PostMapping("/storekeepers/{storekeeper-id}/products")
    public String addProductToStorekeeper(@PathVariable("storekeeper-id") Integer storekeeperId, @RequestParam("productId") Integer productId) {
        storekeeperService.addProductToStorekeeper(storekeeperId, productId);
        return "redirect:/storekeepers/" + storekeeperId + "/products";
    }

    @PostMapping("/storekeepers/{storekeeper-id}/products/delete")
    public String deleteProductToStorekeeperAndCustomer(@PathVariable("storekeeper-id") Integer storekeeperId, @RequestParam("productId") Integer productId) {
        storekeeperService.deleteProductToStorekeeperAndCustomer(productId);
        return "redirect:/storekeepers/" + storekeeperId + "/products";

    }
}
