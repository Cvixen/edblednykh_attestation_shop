package ru.pcs.web.ahop.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SellerForm {
    private String entity;
    private String phone;
    private String email;
    private String adress;
}

