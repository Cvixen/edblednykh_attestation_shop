package ru.pcs.web.ahop.services;

import ru.pcs.web.ahop.forms.StorekeeperForm;
import ru.pcs.web.ahop.models.Product;
import ru.pcs.web.ahop.models.Storekeeper;

import java.util.List;

public interface StorekeeperService {
    Storekeeper addStorekeeper(StorekeeperForm form);

    List<Storekeeper> getAllStorekeeper();

    void deleteStorekeeper(Integer storekeeperId);

    Storekeeper updateStorekeeper(Integer storekeeperId, StorekeeperForm form);

    Storekeeper getStorekeeper(Integer StorekeeperId);

    List<Product> getProductByCustomerAndStorekeeper(Integer storekeeperId);

    List<Product> getProductByCustomerWithoutStorekeeper();

    Product addProductToStorekeeper(Integer storekeeperId, Integer productId);

    Product deleteProductToStorekeeperAndCustomer(Integer productId);

}