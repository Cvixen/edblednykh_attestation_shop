package ru.pcs.web.ahop.services;

import ru.pcs.web.ahop.forms.SignUpForm;

public interface SignUpService {
    void signUpSeller(SignUpForm form);
}
