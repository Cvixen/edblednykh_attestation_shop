package ru.pcs.web.ahop.services;

import ru.pcs.web.ahop.forms.SellerForm;
import ru.pcs.web.ahop.models.Product;
import ru.pcs.web.ahop.models.Seller;

import java.util.List;

public interface SellersService {
    Seller addSeller(SellerForm form);

    List<Seller> getAllSellers();

    void deleteSeller(Integer sellerId);

    Seller updateSeller(Integer sellerId, SellerForm form);

    Seller getSeller(Integer sellerId);

    List<Product> getProductsBySeller(Integer sellerId);

    List<Product> getProductWithoutSeller();

    Product addProductToSeller(Integer sellerId, Integer productId);

    Product deleteProductToSeller(Integer productId);
}