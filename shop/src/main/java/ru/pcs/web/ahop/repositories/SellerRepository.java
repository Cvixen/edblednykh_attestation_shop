package ru.pcs.web.ahop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.ahop.models.Customer;
import ru.pcs.web.ahop.models.Seller;

import java.util.Optional;

public interface SellerRepository extends JpaRepository<Seller, Integer> {

    Optional<Seller> findByEmail(String email);
}
