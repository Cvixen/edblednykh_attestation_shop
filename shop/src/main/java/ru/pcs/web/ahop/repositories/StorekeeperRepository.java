package ru.pcs.web.ahop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.ahop.models.Storage;
import ru.pcs.web.ahop.models.Storekeeper;

public interface StorekeeperRepository extends JpaRepository<Storekeeper, Integer> {
}
