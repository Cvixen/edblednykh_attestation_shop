package ru.pcs.web.ahop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.ahop.models.Customer;
import ru.pcs.web.ahop.models.Product;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
}
