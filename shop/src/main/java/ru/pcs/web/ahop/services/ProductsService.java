package ru.pcs.web.ahop.services;

import ru.pcs.web.ahop.forms.ProductForm;
import ru.pcs.web.ahop.models.Product;

import java.util.List;

public interface ProductsService {
    Product addProduct(ProductForm form);

    List<Product> getAllProducts();

    void deleteProduct(Integer productId);

    Product updateProduct(Integer productId, ProductForm form);

    Product getProduct(Integer productId);
}