package ru.pcs.web.ahop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.ahop.forms.ProductForm;
import ru.pcs.web.ahop.forms.SellerForm;
import ru.pcs.web.ahop.models.Product;
import ru.pcs.web.ahop.models.Seller;
import ru.pcs.web.ahop.services.ProductsService;
import ru.pcs.web.ahop.services.SellersService;

import java.util.List;

@Controller
public class SellersController {

    private final SellersService sellersService;

    @Autowired
    public SellersController(SellersService sellersService) {
        this.sellersService = sellersService;
    }

    @GetMapping("/sellers")
    public String getSellersPage(Model model) {
        List<Seller> sellers = sellersService.getAllSellers();
        model.addAttribute("sellers", sellers);
        return "sellers";
    }

    @GetMapping("/sellers/{seller-id}")
    public String getSellerPage(Model model, @PathVariable("seller-id") Integer sellerId) {
        Seller seller = sellersService.getSeller(sellerId);
        model.addAttribute("seller", seller);
        return "seller"; // это из templates
    }

    @PostMapping("/sellers")
    public String addSeller(SellerForm form) {
        sellersService.addSeller(form);
        return "redirect:/sellers";
    }

    @PostMapping("/sellers/{seller-id}/delete")
    public String deleteSeller(@PathVariable("seller-id") Integer sellerId) {
        sellersService.deleteSeller(sellerId);
        return "redirect:/sellers";
    }

    //изменение
    @PostMapping("/sellers/{seller-id}/update")
    public String updateSeller(@PathVariable("seller-id") Integer sellerId, SellerForm form) {
        sellersService.updateSeller(sellerId, form);
        return "redirect:/sellers";
    }

    @GetMapping("/sellers/{seller-id}/products")
    public String getProductsBySeller(Model model, @PathVariable("seller-id") Integer sellerId) {
        List<Product> products = sellersService.getProductsBySeller(sellerId);
        List<Product> unusedProducts = sellersService.getProductWithoutSeller();
        model.addAttribute("sellerId", sellerId);
        model.addAttribute("products", products);
        model.addAttribute("unusedProducts", unusedProducts);
        return "products_of_seller";
    }

    @PostMapping("/sellers/{seller-id}/products")
    public String addProductToSeller(@PathVariable("seller-id") Integer sellerId, @RequestParam("productId") Integer productId) {
        sellersService.addProductToSeller(sellerId, productId);
        return "redirect:/sellers/" + sellerId + "/products";
    }

    @PostMapping("/sellers/{seller-id}/products/delete")
    public String deleteProductToSeller(@PathVariable("seller-id") Integer sellerId, @RequestParam("productId") Integer productId) {
        sellersService.deleteProductToSeller(productId);
        return "redirect:/sellers/" + sellerId + "/products";
    }
}

