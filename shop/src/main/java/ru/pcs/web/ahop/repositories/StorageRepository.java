package ru.pcs.web.ahop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.ahop.models.Seller;
import ru.pcs.web.ahop.models.Storage;

public interface StorageRepository extends JpaRepository<Storage, Integer> {
}
