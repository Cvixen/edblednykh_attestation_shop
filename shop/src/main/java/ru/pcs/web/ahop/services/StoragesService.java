package ru.pcs.web.ahop.services;

import ru.pcs.web.ahop.forms.StorageForm;
import ru.pcs.web.ahop.models.Storage;

import java.util.List;

public interface StoragesService {
    Storage addStorage(StorageForm form);

    List<Storage> getAllStorage();

    void deleteStorage(Integer storageId);

    Storage updateStorage(Integer storageId, StorageForm form);

    Storage getStorage(Integer storageId);
}