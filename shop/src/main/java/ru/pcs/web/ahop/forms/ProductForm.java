package ru.pcs.web.ahop.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.web.ahop.models.Storekeeper;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductForm {
    private String name;
    private Integer cost;
    private Integer amount;
}
