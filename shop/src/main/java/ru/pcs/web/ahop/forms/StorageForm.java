package ru.pcs.web.ahop.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StorageForm {
    private String name;
    private Integer amount_order;
    private Integer amount_storekeeper;
}
