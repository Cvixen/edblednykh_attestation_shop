package ru.pcs.web.ahop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pcs.web.ahop.forms.StorageForm;
import ru.pcs.web.ahop.models.Storage;
import ru.pcs.web.ahop.repositories.StorageRepository;

import java.util.List;

@Component
public class StoragesServiceImpl implements StoragesService {
    private final StorageRepository storageRepository;

    @Autowired
    public StoragesServiceImpl(StorageRepository storageRepository) {
        this.storageRepository = storageRepository;
    }

    @Override
    public Storage addStorage(StorageForm form) {
        Storage storage = Storage.builder()
                .name(form.getName())
                .amount_order(form.getAmount_order())
                .amount_storekeeper(form.getAmount_storekeeper())
                .build();

        return storageRepository.save(storage);
    }

    @Override
    public List<Storage> getAllStorage() {
        return storageRepository.findAll();
    }

    @Override
    public void deleteStorage(Integer storageId) {
        storageRepository.deleteById(storageId);
    }

    @Override
    public Storage updateStorage(Integer storageId, StorageForm form) {
        Storage storage = Storage.builder()
                .id(storageId)
                .name(form.getName())
                .amount_order(form.getAmount_order())
                .amount_storekeeper(form.getAmount_storekeeper())
                .build();

        return storageRepository.save(storage);
    }

    @Override
    public Storage getStorage(Integer storageId) {
        return storageRepository.getById(storageId);
    }
}
