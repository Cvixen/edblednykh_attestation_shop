package ru.pcs.web.ahop.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.web.ahop.forms.SellerForm;
import ru.pcs.web.ahop.models.Product;
import ru.pcs.web.ahop.models.Seller;
import ru.pcs.web.ahop.repositories.ProductRepository;
import ru.pcs.web.ahop.repositories.SellerRepository;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SellersServiceImpl implements SellersService {
    private final SellerRepository sellerRepository;
    final ProductRepository productRepository;

    @Override
    public Seller addSeller(SellerForm form) {
        Seller seller = Seller.builder()
                .entity(form.getEntity())
                .phone(form.getPhone())
                .email(form.getEmail())
                .adress(form.getAdress())
                .build();
        return sellerRepository.save(seller);
    }

    @Override
    public List<Seller> getAllSellers() {
        return sellerRepository.findAll();
    }

    @Override
    public void deleteSeller(Integer sellerId) {
        sellerRepository.deleteById(sellerId);
    }

    @Override
    public Seller updateSeller(Integer sellerId, SellerForm form) {
        Seller seller = Seller.builder()
                .id(sellerId)
                .entity(form.getEntity())
                .phone(form.getPhone())
                .email(form.getEmail())
                .adress(form.getAdress())
                .build();
        return sellerRepository.save(seller);
    }

    @Override
    public Seller getSeller(Integer sellerId) {
        return sellerRepository.getById(sellerId);
    }

    @Override
    public List<Product> getProductsBySeller(Integer sellerId) {
        return productRepository.findAllBySeller_Id(sellerId);
    }

    @Override
    public List<Product> getProductWithoutSeller() {
        return productRepository.findAllBySellerIsNull();
    }

    @Override
    public Product addProductToSeller(Integer sellerId, Integer productId) {
        Seller seller = sellerRepository.getById(sellerId);
        Product product = productRepository.getById(productId);
        product.setSeller(seller);
        productRepository.save(product);
        sellerRepository.save(seller);
        return product;

    }

    @Override
    public Product deleteProductToSeller(Integer productId) {
        Product product = productRepository.getById(productId);
        product.setSeller(null);
        return productRepository.save(product);
    }
}
