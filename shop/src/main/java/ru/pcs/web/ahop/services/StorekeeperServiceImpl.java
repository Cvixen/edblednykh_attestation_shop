package ru.pcs.web.ahop.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.web.ahop.forms.StorekeeperForm;
import ru.pcs.web.ahop.models.Product;
import ru.pcs.web.ahop.models.Storekeeper;
import ru.pcs.web.ahop.repositories.ProductRepository;
import ru.pcs.web.ahop.repositories.StorekeeperRepository;

import java.util.List;

@Component
@RequiredArgsConstructor
public class StorekeeperServiceImpl implements StorekeeperService {
    private final StorekeeperRepository storekeeperRepository;
    private final ProductRepository productRepository;


    @Override
    public Storekeeper addStorekeeper(StorekeeperForm form) {
        Storekeeper storekeeper = Storekeeper.builder()
                .first_Name(form.getFirst_Name())
                .last_Name(form.getLast_Name())
                .phone(form.getPhone())
                .build();

        return storekeeperRepository.save(storekeeper);
    }

    @Override
    public List<Storekeeper> getAllStorekeeper() {
        return storekeeperRepository.findAll();
    }

    @Override
    public void deleteStorekeeper(Integer storekeeperId) {
        storekeeperRepository.deleteById(storekeeperId);
    }

    @Override
    public Storekeeper updateStorekeeper(Integer storekeeperId, StorekeeperForm form) {
        Storekeeper storekeeper = Storekeeper.builder()
                .id(storekeeperId)
                .first_Name(form.getFirst_Name())
                .last_Name(form.getLast_Name())
                .phone(form.getPhone())
                .build();

        return storekeeperRepository.save(storekeeper);
    }

    @Override
    public Storekeeper getStorekeeper(Integer storekeeperId) {
        return storekeeperRepository.getById(storekeeperId);
    }

    @Override
    public List<Product> getProductByCustomerAndStorekeeper(Integer storekeeperId) {
        return productRepository.findAllByCustomerNotNullAndStorekeeper_Id(storekeeperId);
    }

    @Override
    public List<Product> getProductByCustomerWithoutStorekeeper() {
        return productRepository.findAllByCustomerNotNullAndStorekeeperIsNull();
    }

    @Override
    public Product addProductToStorekeeper(Integer storekeeperId, Integer productId) {
        Product product = productRepository.getById(productId);
        Storekeeper storekeeper = storekeeperRepository.getById(storekeeperId);
        product.setStorekeeper(storekeeper);
        return productRepository.save(product);
    }

    @Override
    public Product deleteProductToStorekeeperAndCustomer(Integer productId) {
        Product product = productRepository.getById(productId);
        product.setCustomer(null);
        product.setStorekeeper(null);
        return productRepository.save(product);
    }


}
