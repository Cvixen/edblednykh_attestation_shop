package ru.pcs.web.ahop.repositories;

import org.hibernate.query.criteria.internal.predicate.PredicateImplementor;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.ahop.models.Customer;
import ru.pcs.web.ahop.models.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    List<Product> findAllByCustomer_Id(Integer customerId);

    List<Product> findAllByCustomerIsNull();

    List<Product> findAllBySeller_Id(Integer sellerId);

    List<Product> findAllBySellerIsNull();

    List<Product> findAllByCustomerNotNullAndStorekeeper_Id(Integer storekeeperId);

    List<Product> findAllByCustomerNotNullAndStorekeeperIsNull();
}
